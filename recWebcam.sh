#!/bin/bash
#you can use your own model by navigating to /dev/v4l/by-id/
v4l2-ctl -d /dev/v4l/by-id/usb-Sonix_Technology_Co.__Ltd._USB_2.0_Camera-video-index0 -c brightness=0,contrast=32,saturation=64,white_balance_temperature_auto=1,exposure_auto=3,exposure_auto_priority=1,gain=0,sharpness=3 --set-fmt-video width=432,height=240,pixelformat=0

#FONTFILE="/usr/share/fonts/dejavu-sans-fonts/DejaVuSans.ttf"
#DRAWTEXT="fontfile=$FONTFILE:text='$(date +"%d-%b-%Y_%H.%M.%S%p")':fontcolor=white:x=w-tw-10:y=10:fontsize=10"
#fontcolor=white:fontsize=24:x=(w-tw)/2:y=(h/PHI)+th, format=yuv420p
#xterm -e pavucontrol &
#I run it on a terminal, options may vary from (-x) using your preffered terminal or maybe you run it in the background, I can stop the recording by pressing p on the xfce4-terminal. 
xfce4-terminal -x ffmpeg -f alsa -thread_queue_size 8192 -i default -f v4l2 -input_format mjpeg -thread_queue_size 1024 -i /dev/v4l/by-id/usb-Sonix_Technology_Co.__Ltd._USB_2.0_Camera-video-index0 -r 15 -s 432x240 \
 -vf "vflip,hflip, drawtext=fontfile=/usr/share/fonts/truetype/DroidSans.ttf: text='%{localtime}':\
 x=(w-tw)/2: y=10: fontcolor=white: box=1: boxcolor=0x00000000@1" \
 -c:v libx264 -crf 26 -preset veryfast -c:a flac /tmp/$(date +"%d-%b-%Y_%H.%M.%S%p").mkv 
#I disabled this option to activate screensaver after 300 seconds of runing the command. 
#sleep 300; xfce4-screensaver-command --activate || true
#if youre a debian based distrto the /tmp folder will be deleted, if you wanna keep your recording make sure to
#to save in a different location your your /Videos home folder.
